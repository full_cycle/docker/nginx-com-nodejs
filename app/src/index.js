const Express = require('express');
const mysql = require('mysql')

const API_PORT = 3000;
const DB_CONFIG = {
  host: 'db',
  user: 'root',
  password: 'root',
  database: 'nodedb'
};

const app = Express();

const executeSQL = async (sql, callback) => {
  return new Promise((resolve, reject) => {
    const connection = mysql.createConnection(DB_CONFIG);
    connection.query(sql, (err, results) => {
      if (err) {
        reject(err);
      } else {
        resolve(results);
      }
    })
    connection.end();
  })
}

const sql = `INSERT INTO people(name) values('VITOR')`
executeSQL(sql);

app.get('/', async (req,res) => {
  const sql = `SELECT name FROM people`;
  const results = await executeSQL(sql)
  const names = results ? results.map(r => r.name).join('<br/>') : 'No results'
  res.send(`<h1>Full Cycle Rocks!</h1><br/>${names}`)
})

app.listen(API_PORT, ()=> {
    console.log('Rodando na porta ' + API_PORT)
})